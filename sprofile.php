                <?php
  include 'connection.php';
  session_start();
$id=$_SESSION['id'];
$query=mysqli_query($conn,"SELECT * FROM provider where id='$id'")or die(mysqli_error());
$row=mysqli_fetch_array($query);
if(!isset($_SESSION['id'])){
  echo "You are logged out";
  header('location:index.html');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Service</title>
 <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500&display=swap" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
<!-- Swiper JS CSS -->
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<!-- custom css -->
<link rel="stylesheet" href="css/style1.css">

</head>
<body>
    <!--<header> here  -->
        <header class="header">
            <div class="container">
                <!--Navbar-->
<nav class="navbar navbar-expand-lg ">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#"><span class="color-primary">Home</span>Service</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="fas fa-bars"></span>
  </button>

 
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item ">
        <a class="nav-link active" href="#" >Home
      
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#ourservice-section">Our Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#about-section">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#complaint">Complaint</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#footer">Contact Us</a>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#serviceprofile">Profile</a>
         
          <a class="dropdown-item" href="servicelogout.php">Logout</a>
        </div>
      </li>
      <li class="nav-item">
        <button class="btn btn-theme waves-effect waves-light" ><a class="waves-effect waves-light" style="color:white;" href="#pag">Partner With Us</a></button>
      </li>
     </ul>
  </div>
</nav>
</div>
</header>
   <!-- </header here -->

   
  <!-- hero BannerHere -->
    <section id="hero-banner">
     <div class="container">
       <div class="row">
         <div class="col-md-5 my-auto mr-auto">
           <h1 class="h1 h1-responsive">Let's Find Service Provider</h1>
           <p>Carpenter, Painter & Plumber.</p>
          <a href="#search-section"><button class="btn btn-theme-2">Find Services</button></a>
         <!--   <button class="btn btn-play ml-3"><i class="fas fa-play"></i></button> -->
         </div>
         <div class="col-md-5 ml-auto">
           <div class="image">
             <img src="img/bg6.jpg" alt="">
           </div>
         </div>
       </div>
     </div>



    </section>


  <!-- /hero BannerHere -->

<!-- Search section -->

<!-- <section id="search-section" class="my-5">
  <div class="container">
    <div class="col-md-11 mx-auto search-area shadow">

      <div class="row">
        <div class="col-md-3">
        <select>
          <option value="">Alkapuri
          </option>
          <option value="">Atladara
          </option>
          <option value="">Alwanaka
          </option>
          <option value="">Manjalpur
          </option>
          <option value="">Makarpura
          </option>
          <option value="">Vadsar
          </option>
        </select>
        <input type="text" class="form-control" placeholder="Area">
      </div>
        <div class="col-md-3">
        <select>
          <option value="">Plumber
          </option>
          <option value="">Carpenter
          </option>
          <option value="">Painter
          </option>
        </select>
        <input type="text" class="form-control" placeholder="Service Provider">
      </div>
       
        <div class="col-md-2 ml-auto my-auto"></div>
        <button class="btn btn-theme"  data-toggle="modal" data-target="#">Search</button>
      </div>
    </div>
  </div>

</section> -->
<!-- /Search section -->

<!-- About Section -->

<section id="about-section" class="my-5">
   <div class="container">
     <div class="row">
       <div class="col-md-6">
         <div class="image">
         <img src="img/about.jpg" alt="">
       </div>
      </div>
       <div class="col-md-5 pl-5">
         <h6 class="h6 color-primary m-0">About Us</h6>
         <h1 class="h1 h1-responsive mb-4">We Provide The Best Home Service Providers For You!</h1>
         <p>
           Home Service is very useful for everyone who wants to offer home services because now a day’s everyone wants to save time and shot out their problems within time without any problem, therefore, online home services are very useful for people. <br><br>
           the user can see a list of home services and book it as per his requirements. The online home service project consists of many categories and services.
         </p>
         <!-- <button class="btn btn-theme-2">Learn More</button> -->
         <!--   <button class="btn btn-play ml-3"><i class="fas fa-play"></i></button> -->
       </div>
     </div>
   </div>


</section>

<!-- /About Section -->










<!-- Our Services -->
<section id="ourservice-section">
  <h1 class="text-center text-secondary mt-4"><a name="ourservice">Our Service</a></h1>

  <div class="container">
    <div class="row">
      
        <div class="col-md-3 m-4">
            <div class="card-wrapper">
                <div class="content">
              
                     <div class="face-front z-depth-2">
                         <img src="img/pen.png" alt="" class="rounded-circle m-2" width="150px" height="150px">
                         <div class="card-body">
                             <h4 class="font-weight-bold">Painter</h4><hr>
                             <p class="font-weight-bold blue-text">Home Service</p>
                             <p>an artist who paints pictures. a person who coats walls or other surfaces with paint, especially as an occupation.</p>
                           </div>
                     </div>

                  <div class="face-back z-depth-2">
                      <div class="card-body">
                          <h4 class="font-weight-bold">Services</h4>
                          <hr>
                          <p>Preparing painting surfaces by washing walls, repairing holes, or removing old paint</p>
                          <p>Mixing, matching, and applying paints </p>
                          <hr>
                          
                           <p>Visiting Charges Applicable:150Rs*</p>
                           <h5 class="font-weight-bold">Painter</h5>

                           <!-- <button class="btn btn-danger ml-1"><a class="nav-link disabled" href="">Book</a></button> -->

                      </div>
                  </div>



                </div>
            </div> 
        </div>

<!-- second -->
<div class="col-md-3 m-4">
<div class="card-wrapper">
    <div class="content">
  
         <div class="face-front z-depth-2">
             <img src="img/car.png" alt="" class="rounded-circle m-2" width="150px" height="150px">
             <div class="card-body">
                 <h4 class="font-weight-bold">Carpenter</h4><hr>
                 <p class="font-weight-bold blue-text">Home Service</p>
                 <p>a worker who builds or repairs wooden structures or their structural parts.</p>
               </div>
         </div>

      <div class="face-back z-depth-2">
          <div class="card-body">
              <h4 class="font-weight-bold">Services</h4>
              <hr>
              <p>Measuring, marking up, cutting, shaping, fitting and finishing timber</p>
              <p>Fitting interiors such as staircases, doors, skirting boards, cupboards and kitchens</p>
              <hr>
              
               <p>Visiting Charges Applicable:150Rs*</p>
               <h5 class="font-weight-bold">Carpenter</h5>

               <!-- <button class="btn btn-danger ml-1"><a class="nav-link disabled" href="">Book</a></button> -->

          </div>
      </div>



    </div>
</div> 
</div>

<!-- third -->
<div class="col-md-3 m-4">
<div class="card-wrapper">
    <div class="content">
  
         <div class="face-front z-depth-2">
             <img src="img/plu.png" alt="" class="rounded-circle m-2" width="150px" height="150px">
             <div class="card-body">
                 <h4 class="font-weight-bold">Plumber</h4><hr>
                 <p class="font-weight-bold blue-text">Home Service</p>
                 <p> a person whose job is to prevent or put an end to leaks of sensitive information.</p>
               </div>
         </div>

      <div class="face-back z-depth-2">
          <div class="card-body">
              <h4 class="font-weight-bold">Services</h4>
              <hr>
              <p>Assemble, install, or repair pipes, fittings, or fixtures of heating, water, or drainage systems.
              </p>
              <p>
                Fill pipes or plumbing fixtures with water .
              </p>
              <hr>
              
               <p>Visiting Charges Applicable:150Rs*</p>
               <h5 class="font-weight-bold">Plumber</h5>

               <!-- <button class="btn btn-danger ml-1"><a class="nav-link disabled" href="#">Book</a></button> -->

          </div>
      </div>



    </div>
</div> 
</div>

<!-- third end -->
        
    </div>
</div>






</section>

<!-- /Our Services -->

<!-- Mail Section -->

<section id="mail-section">
  <div class="container">
    <h1 class="h1 h1-responsive mb-4">Have you any Question?<br>Let us help you</h1>
    <div class="col-md-8 mx-auto form shadow mt-3">
      <form method="post" action="mail.php">
      <div class="row">
        <div class="col-md-9 my-auto">
          <input type="text" class="form-control" name="mail" placeholder="yourmail@domain.com" autocomplete="off">
        </div>
        <div class="col-md-3 text-right">
          <button class="btn btn-theme" type="submit" name="submit">Send</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</section>




<!-- /Mail Section -->

<!-- Footer Section -->

<footer id="footer">

  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <h4 class="h3">Home Service</h4>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Quick Links</h6>
        <hr color="#ffffff">
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Our Services</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Complaint</a></li>
          
        </ul>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Location</h6>
        <hr color="#ffffff">
        <ul>
          <li>Alkapuri</li>
          <li>Atladara</li>
          <li>Manjalpur</li>
          <li>Makarpura</li>
          <li>Vadsar</li>
          
          
        </ul>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Services</h6>
        <hr color="#ffffff">
        <ul>
          <li><a href="#">Painter</a></li>
          <li><a href="#">Carpenter</a></li>
          <li><a href="#">Plumber</a></li>
          
          
        </ul>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Contact</h6>
        <hr color="#ffffff">
        <ul>
          <li>+91-8140970726</li>
          <li>homeservice@gmail.com</li>
          
          
        </ul>
      </div>
    </div>
  </div>


</footer>



<!-- /Footer Section -->

<!-- Modal Complaint -->
<div class="modal fade" id="complaint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Complaint</h5>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        Complain Number<br>
        +91 8140970726  <br>
       
      </div>
      
    </div>
  </div>
</div>
<!-- /Modal Complaint -->







<!-- /Modal User Signup -->
<!-- Modal User Signin -->

<!-- Modal -->


<div class="modal fade" id="serviceprofile" tabindex="-1" role="dialog" aria-labelledby="serviceprofileModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Profile</h5>
        
      </div>
      <div class="modal-body">
       <form method="post" action="#" >
          <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" style="width:20em;" placeholder="Enter your Name" value="<?php echo $row['name']; ?>" required />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" style="width:20em;" placeholder="Enter your Email" required value="<?php echo $row['email']; ?>" />
          </div>
          <div class="form-group">
            <label>Contact</label>
            <input type="contact" class="form-control" name="contact" style="width:20em;" placeholder="Enter your Age" value="<?php echo $row['contact']; ?>">
          </div>
          <div class="form-group">
            <label>Address1</label>
            <input type="text" class="form-control" name="add1" style="width:20em;" required placeholder="Enter your Address1" value="<?php echo $row['add1']; ?>">
          </div>
           <div class="form-group">
            <label>Address2</label>
            <input type="text" class="form-control" name="add2" style="width:20em;" required placeholder="Enter your Address1" value="<?php echo $row['add2']; ?>">
          </div>
           <div class="form-group">
            <label>area</label>
            <input type="text" class="form-control" name="area" style="width:20em;" required  value="<?php echo $row['area']; ?>">
          </div>
           <div class="form-group">
            <label>Photo</label>
            <input type="text" class="form-control" name="photo" style="width:20em;"  value="<?php echo $row['photo']; ?>">
          </div>
           <div class="form-group">
            <label>Description</label>
            <input type="text" class="form-control" name="descrip" style="width:20em;" required placeholder="Enter your Address1" value="<?php echo $row['descrip']; ?>"></textarea>
          </div>
            <div class="form-group">
            <label>Profession</label>
            <input type="text" class="form-control" name="profession" style="width:20em;" required value="<?php echo $row['profession']; ?>">
          </div>
           <div class="form-group">
            <label>Password</label>
            <input type="text" class="form-control" name="password" style="width:20em;" required value="<?php echo $row['password']; ?>">
          </div>
         
          <div class="form-group">
            <input type="submit" name="submit" class="btn btn-primary" style="width:20em; margin:0;"><br><br>
           
          </div>
        </form>
      </div>
     
    </div>
  </div>
</div>



  





<!-- /Modal User Signin -->



</body>
</html>
  <?php
      if(isset($_POST['submit'])){
       $name=$_POST['name'];
       $email=$_POST['email'];
       $contact=$_POST['contact'];
       $add1=$_POST['add1'];
       $add2=$_POST['add2'];
       $area=$_POST['area'];
       $photo=$_POST['photo'];
       $descrip=$_POST['descrip'];
        $profession=$_POST['profession'];
       $password=$_POST['password'];
      

      $query = "UPDATE provider SET name = '$name',
                      email = '$email', contact = '$contact', add1 = '$add1' ,add2 = '$add2' , area ='$area' , photo='$photo' ,descrip='$descrip' , profession='profession' , password='$password'
                      WHERE id = '$id'";
                    $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                    ?>
                     <script type="text/javascript">
            alert("Update Successfull.");
            window.location = "sprofile.php";
        </script>
        <?php
             }               
?>





<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<!-- Swiper JS Javascript -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- SweetAlert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
