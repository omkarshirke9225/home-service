<?php

include('connection.php');


?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="css/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">Home Service</span>
     <div class="d-grid gap-2 d-md-flex justify-content-md-end">
       <a href="logout.php"><button class="btn btn-secondary me-md-2" type="button">LOGOUT</button></a>
    </div>
  </div>
</nav>

<figure class="text-center">
  <blockquote class="blockquote">
    <p>BOOKINGS</p>
  </blockquote>
 
</figure>

<div class="main-div">
	<h3> BOOKING TABLE</h3>
	<div class="center-div">
		<div class="table-responsive">
			<table class="table-primary table-striped table-bordered mydatable">
				<thead>
					<th>ID</th>
					<th>PROVIDER_ID</th>
					<th>FIRST NAME</th>
					<th>LAST NAME</th>
					<th>CONTACT</th>
					<th>ADDRESS</th>	
					<th>DATE</th>
					<th>PAYMENT</th>
					<th>QUERIES</th>
					<th>PROBLEMS</th>
					<th colspan="2">OPERATION</th>	
				</thead>
				<tbody>

    <?php

     include("connection.php");


     $selectquery = " select * from booking";

     $query = mysqli_query($conn,$selectquery);
     
     while($result = mysqli_fetch_assoc($query)){

    ?>

					<tr>
					<td><?php echo $result['id']; ?></td>
					<td><?php echo $result['provider_id']; ?></td>
					<td><?php echo $result['fname']; ?></td>
					<td><?php echo $result['lname']; ?></td>
					<td><?php echo $result['contact']; ?></td>
					<td><?php echo $result['address']; ?></td>
					<td><?php echo $result['date']; ?></td>
					<td><?php echo $result['payment']; ?></td>
					<td><?php echo $result['queries']; ?></td>
					<td><?php echo $result['problem']; ?></td>
                    <td><a href="bookupdate.php?id=<?php echo $result['id'];?>" data-toggle="tooltip" data-placement="bottom" title="Update"><i class="fas fa-pen-alt"></i></a></td>
                    <td><a href="bookdelete.php?id=<?php  echo $result['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick='return checkdelete()'><i class="fas fa-trash-alt"></i></td>
					</tr>
   <?php
	   
	   }



   ?>				
				</tbody>
			</table>

          <script type="text/javascript">
	
	function checkdelete(){
		return confirm('Are you sure want to Delete this Record');
	}
</script>


         </div>
      </div>
    </div>

</body>
</html>