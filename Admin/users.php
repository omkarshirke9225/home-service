
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Service</title>

<!-- custom css -->

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>
<body>
    <!--<header> here  -->
 <nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">Home Service</span>
     <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <a href="logout.php"><button class="btn btn-secondary me-md-2" type="button">LOGOUT</button></a>
    </div>
  </div>
</nav>
   <!-- </header here -->

   
  <figure class="text-center">
  <blockquote class="blockquote">
    <p>USER DATABASE</p>
  </blockquote>
 
</figure>

<div class="main-div">
	<h3> USERS TABLE</h3>
	<div class="center-div">
		<div class="table-responsive">
			<table border="2"  class="table-primary table-striped table-bordered mydatable">
				<thead>
					<th>ID</th>
					<th>NAME</th>
					<th>EMAIL</th>
					<th>PASSWORD</th>
					<th colspan="2">OPERATION</th>	
				</thead>
				<tbody>

    <?php

     include("connection.php");


     $selectquery = " select * from user";

     $query = mysqli_query($conn,$selectquery);
     
     while($result = mysqli_fetch_assoc($query)){

    ?>

					<tr>
					<td><?php echo $result['id']; ?></td>
					<td><?php echo $result['name']; ?></td>
					<td><?php echo $result['email']; ?></td>
					<td><?php echo $result['password']; ?></td>
					
                    <td>
                    <a href="userupdate.php?id=<?php echo $result['id'];?>" data-toggle="tooltip" data-placement="bottom" title="Update"><i class="fas fa-pen-alt"></i></a></td>
                    <td><a href="userdelete.php?id=<?php  echo $result['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete" onclick='return checkdelete()'><i class="fas fa-trash-alt"></i></td>
					</tr>
   <?php
	   
	   }



   ?>				
				</tbody>
			</table>

<script type="text/javascript">
	
	function checkdelete(){
		return confirm('Are you sure want to Delete this Record');
	}
</script>
         </div>
      </div>
    </div>






    
</body>
</html>
 

















