
                <?php
  include 'connection.php';
  session_start();
$id=$_SESSION['id'];
$query=mysqli_query($conn,"SELECT * FROM user where id='$id'")or die(mysqli_error());
$row=mysqli_fetch_array($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Service</title>
 <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;500&display=swap" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
<!-- Swiper JS CSS -->
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
<!-- custom css -->
<link rel="stylesheet" href="css/style1.css">

</head>
<body>
    <!--<header> here  -->
        <header class="header">
            <div class="container">
                <!--Navbar-->
<nav class="navbar navbar-expand-lg ">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#"><span class="color-primary">Home</span>Service</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="fas fa-bars"></span>
  </button>

 
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item ">
        <a class="nav-link active" href="#" >Home
      
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#ourservice-section">Our Services</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#about-section">About Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#complaint">Complaint</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#footer">Contact Us</a>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <!-- <a class="dropdown-item" href="#" data-toggle="modal" data-target="#userprofile">Profile</a>
          <a class="dropdown-item" href="#">MyBookings</a> -->
          <a class="dropdown-item" href="userlogout.php">Logout</a>
        </div>
      </li>
      
     </ul>
  </div>
</nav>
</div>
</header>


 <!-- hero BannerHere -->
    <section id="hero-banner">
     <div class="container">
       <div class="row">
         <div class="col-md-5 my-auto mr-auto">
           <h1 class="h1 h1-responsive">Let's Find Service Provider</h1>
           <p>In expecting display,thought.</p>
           <button class="btn btn-theme-2">Find Services</button>
          <!--  <button class="btn btn-play ml-3"><i class="fas fa-play"></i></button> -->
         </div>
         <div class="col-md-5 ml-auto">
           <div class="image">
             <img src="img/bg6.jpg" alt="">
           </div>
         </div>
       </div>
     </div>



    </section>


  <!-- /hero BannerHere -->

<!-- booking -->

<?php

include('connection.php');

$id = $_GET['id'];
$result=mysqli_query($conn,"SELECT * FROM provider where id=$id");
while($res=mysqli_fetch_array($result)){
  $photo=$res['photo'];
  $name=$res['name'];
   $area=$res['area'];
   $profession=$res['profession'];
  
}
?>


<section>
  <div class="container" style="margin-top: 30px;">
    <div class="card text-center">
        <div class="card-header">
            <h3>Details about <?php echo $name;?></h3>
        </div>
        <div class="container" style="margin-top: 30px;">
            <div class="row">
                <div class="col">
                    <img style="height: 250px"
                        src="img2/<?php echo $photo;?>">
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Name</th>
                    <td>
                        <?php echo $name;?>
                    </td>
                    <th>Profession</th>
                    <td>
                        <?php echo $profession;?>
                </tr>
                <tr>
                    
                    <th>Area</th>
                    <td>
                       <?php echo $area;?>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>



<div class="container" style="margin-bottom: 60px;margin-top: 20px;">
    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <h3 class="text-center">Book Appointment from <?php echo $name;?>
                </h3>
            </div>
            <hr>

            <form action="ubooking2.php" method="post">
                <input type="hidden" name="provider"
                    value="<?php echo $id; ?>">
                <div class="form-group">
                    <label for="">First Name</label>
                    <input id="fname" name="fname" type="text" class="form-control" placeholder="First Name" required>
                </div>

                <div class="form-group">
                    <label for="">Last Name</label>
                    <input id="lname" name="lname" type="text" class="form-control" placeholder="Last Name" required>
                </div>

                <div class="form-group">
                    <label for="">Contact No.</label>
                    <input id="contact" name="contact" type="text" class="form-control" placeholder="Contact No."
                        minlength="10" maxlength="10"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                </div>

                <div class="form-group">
                    <label for="">Address</label>
                    <input id="address" name="address" type="text" class="form-control" placeholder="Address"
                        maxlength="255" required>
                </div>

                <div class="form-group">
                    <label for="">Date</label>
                    <input class="form-control" type="date" name="date" id="date" required>
                </div>

                <div class="form-group">
                    <label for="">Payment Mode</label>
                    <select class="form-control" name="payment" id="payment" required>
                        <option value="cash">Cash</option>
                        
                    </select>
                </div>

                <div class="form-group">
                    <label for="">Queries</label>
                    <textarea id="queries" name="queries" class="form-control" maxlength="255"
                        placeholder="Any queries..?"></textarea>
                </div>
                 <div class="form-group">
                    <label for="">Problem</label>
                    <select class="form-control" name="problem" id="problem">
                        
                        <option value="Manjor Problem">Major Problem</option>
                    
                        <option value="Minor Problem">Minor Problem</option>
                        
                      

                    </select>
                </div>

                <button style="margin-top: 30px" class="btn btn-block btn-primary" type="submit" name="submit"
                    id="book">Book
                  </button>
            </form>

        </div>
    </div>
</div>

</section>

<!-- /booking -->

<!-- About Section -->

<section id="about-section" class="my-5">
   <div class="container">
     <div class="row">
       <div class="col-md-6">
         <div class="image">
         <img src="img/about.jpg" alt="">
       </div>
      </div>
       <div class="col-md-5 pl-5">
         <h6 class="h6 color-primary m-0">About Us</h6>
         <h1 class="h1 h1-responsive mb-4">We Provide The Best Home Service Providers For You!</h1>
         <p>
           Home Service is very useful for everyone who wants to offer home services because now a day’s everyone wants to save time and shot out their problems within time without any problem, therefore, online home services are very useful for people. <br><br>
           the user can see a list of home services and book it as per his requirements. The online home service project consists of many categories and services
         </p>
         <button class="btn btn-theme-2">Find Services</button>
           <!-- <button class="btn btn-play ml-3"><i class="fas fa-play"></i></button> -->
       </div>
     </div>
   </div>


</section>

<!-- /About Section -->


<!-- Our Services -->
<section id="ourservice-section">
  <h1 class="text-center text-secondary mt-4"><a name="ourservice">Our Service</a></h1>

  <div class="container">
    <div class="row">
      
        <div class="col-md-3 m-4">
            <div class="card-wrapper">
                <div class="content">
              
                     <div class="face-front z-depth-2">
                         <img src="img/pen.png" alt="" class="rounded-circle m-2" width="150px" height="150px">
                         <div class="card-body">
                             <h4 class="font-weight-bold">Painter</h4><hr>
                             <p class="font-weight-bold blue-text">Home Service</p>
                             <p>an artist who paints pictures. a person who coats walls or other surfaces with paint, especially as an occupation.</p>
                           </div>
                     </div>

                  <div class="face-back z-depth-2">
                      <div class="card-body">
                          <h4 class="font-weight-bold">Services</h4>
                          <hr>
                          <p>Preparing painting surfaces by washing walls, repairing holes, or removing old paint</p>
                          <p>Mixing, matching, and applying paints </p>
                          <hr>
                          
                           <p>Visiting Charges Applicable:150Rs*</p>
                           <h5 class="font-weight-bold">Painter</h5>

                           <button class="btn btn-danger ml-1"><a class="nav-link disabled" href="">Book</a></button>

                      </div>
                  </div>



                </div>
            </div> 
        </div>

<!-- second -->
<div class="col-md-3 m-4">
<div class="card-wrapper">
    <div class="content">
  
         <div class="face-front z-depth-2">
             <img src="img/car.png" alt="" class="rounded-circle m-2" width="150px" height="150px">
             <div class="card-body">
                 <h4 class="font-weight-bold">Carpenter</h4><hr>
                 <p class="font-weight-bold blue-text">Home Service</p>
                 <p>a worker who builds or repairs wooden structures or their structural parts.</p>
               </div>
         </div>

      <div class="face-back z-depth-2">
          <div class="card-body">
              <h4 class="font-weight-bold">Services</h4>
              <hr>
              <p>Measuring, marking up, cutting, shaping, fitting and finishing timber</p>
              <p>Fitting interiors such as staircases, doors, skirting boards, cupboards and kitchens</p>
              <hr>
              
               <p>Visiting Charges Applicable:150Rs*</p>
               <h5 class="font-weight-bold">Carpenter</h5>

               <button class="btn btn-danger ml-1"><a class="nav-link disabled" href="">Book</a></button>

          </div>
      </div>



    </div>
</div> 
</div>

<!-- third -->
<div class="col-md-3 m-4">
<div class="card-wrapper">
    <div class="content">
  
         <div class="face-front z-depth-2">
             <img src="img/plu.png" alt="" class="rounded-circle m-2" width="150px" height="150px">
             <div class="card-body">
                 <h4 class="font-weight-bold">Plumber</h4><hr>
                 <p class="font-weight-bold blue-text">Home Service</p>
                 <p> a person whose job is to prevent or put an end to leaks of sensitive information.</p>
               </div>
         </div>

      <div class="face-back z-depth-2">
          <div class="card-body">
              <h4 class="font-weight-bold">Services</h4>
              <hr>
              <p>Assemble, install, or repair pipes, fittings, or fixtures of heating, water, or drainage systems.
              </p>
              <p>
                Fill pipes or plumbing fixtures with water .
              </p>
              <hr>
              
               <p>Visiting Charges Applicable:150Rs*</p>
               <h5 class="font-weight-bold">Plumber</h5>

               <button class="btn btn-danger ml-1"><a class="nav-link disabled" href="#">Book</a></button>

          </div>
      </div>



    </div>
</div> 
</div>

<!-- third end -->
        
    </div>
</div>






</section>

<!-- /Our Services -->


<!-- Mail Section -->

<section id="mail-section">
  <div class="container">
    <h1 class="h1 h1-responsive mb-4">Have you any Question?<br>Let us help you</h1>
    <div class="col-md-8 mx-auto form shadow mt-3">
      <div class="row">
        <div class="col-md-9 my-auto">
          <input type="text" class="form-control" placeholder="yourmail@domain.com">
        </div>
        <div class="col-md-3 text-right">
          <button class="btn btn-theme">Send</button>
        </div>
      </div>
    </div>
  </div>
</section>




<!-- /Mail Section -->

<!-- Footer Section -->

<footer id="footer">

  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <h4 class="h3">Home Service</h4>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Quick Links</h6>
        <hr color="#ffffff">
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Our Services</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Complaint</a></li>
          
        </ul>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Location</h6>
        <hr color="#ffffff">
        <ul>
          <li>Alkapuri</li>
          <li>Atladara</li>
          <li>Manjalpur</li>
          <li>Makarpura</li>
          <li>Vadsar</li>
          
          
        </ul>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Services</h6>
        <hr color="#ffffff">
        <ul>
          <li><a href="#">Painter</a></li>
          <li><a href="#">Carpenter</a></li>
          <li><a href="#">Plumber</a></li>
          
          
        </ul>
      </div>
      <div class="col-md-2">
        <h6 class="m-0 h6">Contact</h6>
        <hr color="#ffffff">
        <ul>
          <li>+91-8140970726</li>
          <li>homeservice@gmail.com</li>
          
          
        </ul>
      </div>
    </div>
  </div>


</footer>



<!-- /Footer Section -->

<!-- Modal Complaint -->
<div class="modal fade" id="complaint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Complaint</h5>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        Complain Number<br>
        +91 8140970726  <br>
       
      </div>
      
    </div>
  </div>
</div>
<!-- /Modal Complaint -->
</body>
</html>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<!-- Swiper JS Javascript -->
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
